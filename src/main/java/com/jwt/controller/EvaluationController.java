package com.jwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jwt.model.Input;
import com.jwt.model.UserModel;
import com.jwt.service.EvaluationService;

@Controller
@RequestMapping("/register")
public class EvaluationController
{
	private String message_right ;
	private String message_wrong;

	@Autowired
	private EvaluationService eservice;

	@RequestMapping(method = RequestMethod.GET)
	public String form(Model model)
	{
		Input user = new Input();
		model.addAttribute("message_right",message_right);
		model.addAttribute("message_wrong",message_wrong);
		model.addAttribute("userForm",user);
		List<UserModel> users = this.eservice.getAllUsers();
		model.addAttribute("users",users);
		return "form";
		
	}

	@RequestMapping(method = RequestMethod.POST)
	public String resgistration(Model model, @ModelAttribute("userForm") Input user)
	{

		if (this.eservice.verifyQuery(user.getInput()))
			{
			message_wrong=null;
			message_right = "the answer is correct";
			}
		else
		{
			message_right=null;
			message_wrong = "the answer is wrong"; 
			}
		
		return "redirect:/register";
	}
}
