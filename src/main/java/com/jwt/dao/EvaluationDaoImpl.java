package com.jwt.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.UserModel;
import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;

@Repository
public class EvaluationDaoImpl implements EvaluationDao
{

	@Autowired
	private SessionFactory sessionFactory;

	public List<UserModel> getUserList()
	{
		return sessionFactory.getCurrentSession().createQuery("from UserModel").list();
	}

	@SuppressWarnings("unchecked")
	public List<UserModel> getUserList(String query)
	{
			List<UserModel> list= new ArrayList<UserModel>();
		 try{
			 list = sessionFactory.getCurrentSession().createSQLQuery(query).addEntity(UserModel.class).list();
		 }catch (GenericJDBCException e) {
				e.printStackTrace();
			}
		 catch(SQLGrammarException e){
			 e.printStackTrace(); 
		 }
		 return list ;
		 }
		
}
