package com.jwt.service;

import java.util.List;
import com.jwt.model.UserModel;
import com.jwt.model.UserModel;;
public interface EvaluationService {

	public List<UserModel> getAllUsers();
	public List<UserModel> getAllUsers(String query);
	public boolean verifyQuery(String query);
}
