package com.jwt.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.EvaluationDao;
import com.jwt.model.UserModel;

@Service
@Transactional
public class EvaluationServiceImpl implements EvaluationService
{
	@Autowired
	private EvaluationDao edao;
	public static int size;

	public List<UserModel> getAllUsers()
	{
		size = this.edao.getUserList().size();
		return edao.getUserList();
	}

	public List<UserModel> getAllUsers(String query)
	{
		return edao.getUserList(query);
	}

	public boolean verifyQuery(String query)
	{
		
		List<UserModel> list1 = this.getAllUsers(query);
		List<UserModel> list2 = this.getAllUsers();
		int i = 0;
		for (UserModel u : list2)
		{
			if (list1.size() == list2.size())
			{
				if (!(u.getId()==list1.get(i).getId()))
					return false;
				i++;
			}
			else
				return false;
		}
		return true;
	}

}
