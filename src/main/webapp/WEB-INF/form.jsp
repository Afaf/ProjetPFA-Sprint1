<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet"  />
<title>SQL Evaluation</title>
</head>
<body>
	<div class="container">
		<div class="jumbotron">
			<h1>SQL Evaluation</h1>
			<p>This is a SQL Evaluation </p>
		</div>
		<p>With SQL, how do you select all the records from a table named "UserModel"</p>            
  <table class="table">
    <thead>
      <tr>
        <th>name</th>
        <th>age</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach var="user" items="${users}">
      <tr>
        <td>${user.name}</td>
        <td>${user.age}</td>
      </tr>
	</c:forEach>
    </tbody>
  </table>
	</div>
	<div class="container">
		<form:form action="register" method="POST" commandName="userForm">
			<div class="form-group">
				<label for="comment">your Answer : </label>
				<form:textarea path="input" class="form-control" rows="5"
					id="comment"></form:textarea>
				<br/>
				<button type="submit">OK</button>
			</div>
		</form:form>
	</div>	
	<div class="conatiner">
	<div class="row">
        <div class="col-sm-2">
		<span class="label label-success">${ message_right }</span>
		<span class="label label-danger">${ message_wrong  }</span>
		</div>
		</div>
	</div>
</body>
</html>